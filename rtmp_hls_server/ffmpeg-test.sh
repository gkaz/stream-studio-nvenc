#!/bin/bash

app="live"
name="test"


LD_LIBRARY_PATH=/opt/Shotcut/Shotcut.app/lib

#/opt/Shotcut/Shotcut.app/ffmpeg -codecs
#/opt/Shotcut/Shotcut.app/ffmpeg -filters
#exit 0

/opt/Shotcut/Shotcut.app/ffmpeg -async 1 -vsync -1 -hwaccel_output_format cuda -hwaccel cuvid -c:v h264_cuvid -i rtmp://localhost:1935/${app}/${name} \
						-c:v h264_nvenc -c:a aac -b:v 256k -b:a 64k -zerolatency 1 -f flv rtmp://localhost:1935/show/${name}_low \
						-c:v h264_nvenc -c:a aac -b:v 768k -b:a 128k -zerolatency 1 -f flv rtmp://localhost:1935/show/${name}_mid \
						-c:v h264_nvenc -c:a aac -b:v 1024k -b:a 128k -zerolatency 1 -f flv rtmp://localhost:1935/show/${name}_high \
						-c:v h264_nvenc -c:a aac -b:v 1920k -b:a 128k -zerolatency 1 -f flv rtmp://localhost:1935/show/${name}_hd720 \
						-c copy -f flv rtmp://localhost:1935/show/${name}_src;

